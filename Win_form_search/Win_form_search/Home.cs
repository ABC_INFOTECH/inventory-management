﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Xml;
using System.Data.SQLite;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using iTextSharp.tool.xml;
using System.IO;

namespace Win_form_search
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
            tabControl1.SelectedIndexChanged += new EventHandler(tabControl1_SelectedIndexChanged);
            tabControl1.SelectTab("tabPage1");
            comboBox1.SelectedIndex = 0; 
        }

        private void Home_Load(object sender, EventArgs e)
        {
            CreateLoadDb();

            //string constring = string.Empty;
            ////constring = System.Configuration.ConfigurationSettings.AppSettings["v2"].ToString();
            //constring = System.Configuration.ConfigurationManager.ConnectionStrings["v2"].ToString();

            //using (SqlConnection sqlcon = new SqlConnection(constring))
            //{
            //    sqlcon.Open();
            //    SqlCommand sqlcmd = new SqlCommand("Select artnamn from artikel artnamn where artnamn<>'' order by 1", sqlcon);
            //    SqlDataReader sqlreader = sqlcmd.ExecuteReader();
            //    using(XmlWriter xwriter = XmlWriter.Create("artikel.xml"))
            //    {
            //        xwriter.WriteStartDocument();
            //        xwriter.WriteStartElement("artikels");
            //        while (sqlreader.Read())
            //        {
            //            string artname = sqlreader[0].ToString();
            //            xwriter.WriteElementString("artname", sqlreader[0].ToString());
            //        }
            //         xwriter.WriteEndElement();
            //         xwriter.WriteEndDocument();

            //    }
            //}
            
        }


        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage3"])
            {
                Initialize_SearchTextBox();
            }

            if (tabControl1.SelectedTab == tabControl1.TabPages["tabPage1"])
            {
                //Initialize_AddTextBox();
            }
        }
        private void CreateLoadDb()
        {
            bool fileexists=System.IO.File.Exists("InventoryDb.sqlite");
            if (!fileexists)
            {
                SQLiteConnection.CreateFile("InventoryDb.sqlite");
                using (SQLiteConnection con = new SQLiteConnection("Data source=InventoryDb.sqlite;version=3; UseUTF16Encoding=True;Password=Pass@123"))
                {
                    con.Open();
                    string sql = "Create table if not exists artikels(artikelname varchar(30))";
                    SQLiteCommand cmd = new SQLiteCommand(sql, con);
                    cmd.ExecuteNonQuery();
                    //con.Close();
                }
            }
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Initialize_SearchTextBox()
        {
            AutoCompleteStringCollection stringcoll = new AutoCompleteStringCollection();

            //string constring = string.Empty;
            //constring = System.Configuration.ConfigurationSettings.AppSettings["v2"].ToString();
            //constring = System.Configuration.ConfigurationManager.ConnectionStrings["v2"].ToString();
            //constring = "Data source=InventoryDb.sqlite;version=3; UseUTF16Encoding=True;Password=Pass@123";

            using (SQLiteConnection sqlcon = new SQLiteConnection("Data source=InventoryDb.sqlite;version=3; UseUTF16Encoding=True;Password=Pass@123"))
            {
                sqlcon.Open();
                SQLiteCommand sqlcmd = new SQLiteCommand("Select artikelname from artikels where artikelname <>'' order by 1 ", sqlcon);
                SQLiteDataReader sqlreader = sqlcmd.ExecuteReader();

                while (sqlreader.Read())
                {
                    string artname = sqlreader[0].ToString();
                    stringcoll.Add(sqlreader[0].ToString());
                }
               // sqlcon.Close();
            }
            textBox1.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            textBox1.AutoCompleteSource = AutoCompleteSource.CustomSource;
            textBox1.AutoCompleteCustomSource = stringcoll;

            ///////////////////////////////////////////////////////////////////

        }

        private void button3_Click(object sender, EventArgs e)
        {
            string artname = string.Empty;
            int res = 0;
            artname = textBox2.Text;

            using (SQLiteConnection con = new SQLiteConnection("Data source=InventoryDb.sqlite;version=3; UseUTF16Encoding=True;Password=Pass@123"))
            {
                con.Open();
                string sql = "Insert into artikels values(@artnam)";
                SQLiteCommand cmd = new SQLiteCommand(sql, con);
                cmd.Parameters.AddWithValue("@artnam", artname);
                res=cmd.ExecuteNonQuery();
                //con.Close();
            }
            if (res == 0)
            {
                WriteLog(artname +"Not Updated.Please try again");
            }
            else
                WriteLog(artname+" has been updated in the system.");
        }


        internal void WriteLog(string msg)
        {
            richTextBox2.AppendText("\r\n" + msg);
            richTextBox2.ScrollToCaret();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {

            string shopname = "Bal Bichi Auto Parts";
            string shopaddress = "4th cross street \nVenakteshwara Nagar\nChennai\npin 600089";
            string templatepath = @"C:\Users\Dutta\Documents\visual studio 2013\Projects\Win_form_search\Win_form_search\Resources\invoice_template.html";

            //string dir = Directory.GetParent(Environment.CurrentDirectory).ToString()+templatepath;//Path.GetFullPath(string.Format(templatepath));

            //sbuilder.Append("<div>");
            //sbuilder.Append("<table>");
            //sbuilder.Append("<tr>");
            //sbuilder.Append("<td align='center' style='background-color: #18B5F0'>");
            //sbuilder.Append(shopaddress);
            //sbuilder.Append("</td>");
            //sbuilder.Append("<td align='center' style='background-color: #18B5F0'>");
            //sbuilder.Append(shopname);
            //sbuilder.Append("</td>");
            //sbuilder.Append("</tr>");
            //sbuilder.Append("</table>");
            //sbuilder.Append("</div>");

            //sbuilder.Append("<hr/>");
            //sbuilder.Append("<hr/>");

            //sbuilder.Append("<table width='100%' cellspacing='0' cellpadding='0' border='0.5'>");
            //sbuilder.Append("<tr>");
            //sbuilder.Append("<td align='center' style='background-color: #18B5F0'><b>Description</b></td>");
            //sbuilder.Append("<td><b>Quantity</b>");
            //sbuilder.Append("<td><b>Unit Price</b>");
            //sbuilder.Append("<td><b>Amount</b>");
            //sbuilder.Append("</tr>");
            //sbuilder.Append("</table>");
                String sbuilder = File.ReadAllText(templatepath);







                string invoiceno = "abcd";
            //Convert.ToBase64String(Guid.NewGuid().ToByteArray());
                //string filename = @"C:\Users\Dutta\Documents\visual studio 2013\Projects\Win_form_search\Win_form_search\Reports\Invoices\" + invoiceno + ".pdf";

                string filename = @"C:\Myfolder\Reports\Invoices\" + invoiceno + ".pdf";

            Rectangle page = new Rectangle(PageSize.A4);
            page.BackgroundColor = new BaseColor(System.Drawing.Color.White);


            Document document = new Document(page, 10, 10, 36, 36);//create itextsharp document
            
                FileStream fs = new FileStream(filename, FileMode.Create, FileAccess.ReadWrite);

                PdfWriter writer = PdfWriter.GetInstance(document, fs);//created the pdf document

                

                   // StringReader sr = new StringReader(sbuilder.ToString());
                    //  XMLWorkerHelper.GetInstance().ParseXHtml(writer,document,sr);
                   // HTMLWorker htmlparser = new HTMLWorker(document);
                    document.Open();

                    var font_main = FontFactory.GetFont("Courier", 18, 1, BaseColor.BLUE);
                    var font1 = FontFactory.GetFont("Courier", 10, 1, BaseColor.BLACK);
                    var font2 = FontFactory.GetFont("Courier", 11, 0, BaseColor.BLACK);

                    PdfPTable table = new PdfPTable(2);
                    PdfPCell cell = new PdfPCell(new Phrase("Header spanning 2 columns"));
                    cell.Colspan = 2;
                    cell.Border = 1;
                    cell.HorizontalAlignment = 0; //0=Left, 1=Centre, 2=Right
                    table.AddCell(cell);
                    var t1 = new Paragraph("Bal bichi AutoParts", font_main);
                    table.AddCell(t1);
                  
                    PdfPTable table2 = new PdfPTable(1);
                    PdfPCell cell1 = new PdfPCell();
                    cell1.Colspan = 1;
                    cell1.Rowspan = 2;

                    table2.AddCell("Invoice No:");
                    table2.AddCell("Date:");

                    table.AddCell(table2);
                    document.Add(table);
            /////////////////////////////New table

                    PdfPTable shopadrstable = new PdfPTable(1);
                    var t2 = new Paragraph(shopaddress, font1);
                    PdfPCell cell2 = new PdfPCell();
                    cell2.Colspan = 1;
                    cell2.Border = 1;
                    cell2.HorizontalAlignment = 0;
                    shopadrstable.AddCell(t2);
                    document.Add(shopadrstable);
            ///////////////////////////////////////New table
                    PdfPTable customertable = new PdfPTable(4);
                    PdfPCell cell3head = new PdfPCell(new Phrase("   "));
                    var cusadrs = new Paragraph("Address", font2);
                    var cusname = new Paragraph("Customer Name", font2);
                    var cusph = new Paragraph("Phone No", font2);
                    var cuseml = new Paragraph("Email", font2);
                    PdfPCell cell3 = new PdfPCell();
                    cell3.Colspan = 4;
                    cell3.Border = 0;
                    cell3.HorizontalAlignment = 0;
                    
                    customertable.AddCell(cusname);
                    customertable.AddCell(cusadrs);
                    customertable.AddCell(cusph);
                    customertable.AddCell(cuseml);

                    customertable.AddCell("Kaustav Datta");
                    customertable.AddCell("Chennai\nRamapuram");
                    customertable.AddCell("9500131433");
                    customertable.AddCell("abcd@xyz.com");

                    document.Add(customertable);


                    



                    //XMLWorkerHelper.GetInstance().ParseXHtml(writer, document, sr);
                    //htmlparser.Parse(sr);

                   // .GetInstance().ParseXHtml(writer, doc, srHtml);
                   
                   // fs.Close();
                    document.CloseDocument();
                    document.Close();
                    
            
                
            //document
            //Create test pdf
            //Document document = new Document();
            //PdfWriter.GetInstance(document, new FileStream(@"C:\Myfolder\Reports\Invoices\a.pdf", FileMode.Create));
            //document.Open();
            //Paragraph p = new Paragraph("Test");
            //document.Add(p);
            //document.Close(); 
            
        }

        
    }
}
