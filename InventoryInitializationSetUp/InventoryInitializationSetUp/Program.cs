﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using System.Data.SQLite;

namespace InventoryInitializationSetUp
{
    class Program
    {
        static void Main(string[] args)
        {
            //if (args.Length.Equals(0))
            //{
            //    new Program().ShowArgumentError();
            //    Application.Exit();
            //}
            //else
            //{

            ConfigurationManager.RefreshSection("appSettings");
            string rootfolder = ConfigurationManager.AppSettings["rootPath"];
            string appfolder = ConfigurationManager.AppSettings["appFolder"];
            string rootdir = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86);
            string[] folders = ConfigurationManager.AppSettings["folders"].Split(',');

            Setup set = new Setup(rootdir, rootfolder, appfolder, folders);
            if (set.CreateDirectory())
            {
                if (set.InitializeDb())
                {
                    MessageBox.Show("Basic Setup completed.", "Setup");
                }
            }
            Application.Exit();
            // }

            //MessageBox.Show("Hello");
            //Application.Exit();

        }
        internal void ShowArgumentError()
        {
            string _classname = this.GetType().Name;//Name of current class
            var _namespace = this.GetType().Namespace; // Name of current namespace
            MessageBox.Show("Please add the parameter to the " + _classname + ".exe");
        }
    }

    class Setup
    {
        string rootfolder = String.Empty;
        string rootdir = String.Empty;
        string appfolder = String.Empty;
        string[] folders = new string[30];

        public Setup(string rootdir, string rootfolder, string appfolder, string[] folders)
        {
            // TODO: Complete member initialization
            this.rootdir = rootdir;
            this.rootfolder = rootfolder;
            this.appfolder = appfolder;
            this.folders = folders;
        }
        internal bool CreateDirectory()
        {
            try
            {
                if (!Directory.Exists(Path.Combine(rootdir, rootfolder)))
                {
                    Directory.CreateDirectory(Path.Combine(rootdir, rootfolder));
                }
                foreach (string folder in folders)
                {
                    if (!Directory.Exists(Path.Combine(rootdir, rootfolder, folder)))
                    {
                        Directory.CreateDirectory(Path.Combine(rootdir, rootfolder, folder));
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't create folder stuructures\n" + ex.Message);
                return false;
                //Application.Exit();
            }
            finally
            {
                // dir.
            }

        }

        internal bool InitializeDb()
        {
            SQLiteConnection con = new SQLiteConnection();
            SQLiteCommand cmd = new SQLiteCommand();
            string constring = string.Empty;
            string dbfolder = string.Empty;
            string sqlscript = string.Empty;
            string dbname = String.Empty;

            try
            {
                dbfolder = Path.Combine(rootdir, rootfolder, appfolder);

                dbname = "InventoryDb.sqlite";
                constring = @"Data source=" + dbname + ";version=3; UseUTF16Encoding=True;Password=Pass@123";
                sqlscript = "";
                con.ConnectionString = constring;
                cmd.Connection = con;
                cmd.CommandText = sqlscript;
                con.Open();
                if (!File.Exists(Path.Combine(dbfolder, dbname)))
                {
                    SQLiteConnection.CreateFile(Path.Combine(dbfolder, dbname));
                    cmd.ExecuteNonQuery();
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Can't create database\n" + ex.Message);
                return false;
            }
            finally
            {
                cmd.Dispose();
                con.Close();
            }
        }
    }
}

